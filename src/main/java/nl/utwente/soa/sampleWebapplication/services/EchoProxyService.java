package nl.utwente.soa.sampleWebapplication.services;

import jakarta.websocket.EncodeException;

import java.io.IOException;

public interface EchoProxyService {
    void echo(String message) throws IOException, EncodeException;
}
